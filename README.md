**TOFFA**

***Description***

Toffa is a mini application that allows you to save batches of survey data in a given database and export them in a csv format according to a well-defined criteria.


**Technological stack**

This application is developed with python (***fastapi***) in backend, ***Angular 12***, in frontend and **postgresql 12**, **Celery** and **rabbitmq** for background tasks.
for more details see STCK-README.md


**Possible improvements**

- Refactor certain methods in particular those of files processing  (export/import) in order to reduce complexity.

- Remove useless debug features  

- Add tests for  a good level of coverage
Use asynchronous mode for APIs.

- Review the feedback model (survey data) in order to simplify the transformations.

- Exclude unused  modules from the frontent and backend.


**Deployment for test**
Follow this steps to test the app :
- Install docker and docker-compose
- copy `.env.exemple` file to a file named `.env`
- update the `.env` file content to set env variables 
- run docker-compose up -d 
- open your browser at http://localhost:84 to test the frontend
- got to http://localhost:84/docs (swagger) or http://localhost:84/redoc (redoc) to see the test APIs
- default credentials are 
    - username: admin@toffa.com
    - password: changethis
- see data_import_sample.json  for data import template.

