"""add survey model

Revision ID: 7fd06102361d
Revises: d4867f3a4c0a
Create Date: 2021-12-01 11:40:01.757722

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7fd06102361d'
down_revision = 'd4867f3a4c0a'
branch_labels = None
depends_on = None


def upgrade():
    # ### surey table migration ###
    op.create_table(
        "survey",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("title", sa.String(), nullable=True),
        sa.Column("description", sa.String(), nullable=True),
        sa.Column("owner_id", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(["owner_id"], ["user.id"],),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_survey_description"), "survey", ["description"], unique=False)
    op.create_index(op.f("ix_survey_id"), "survey", ["id"], unique=False)
    op.create_index(op.f("ix_survey_title"), "survey", ["title"], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### drop survey table  ###
    op.drop_index(op.f("ix_survey_title"), table_name="survey")
    op.drop_index(op.f("ix_survey_id"), table_name="survey")
    op.drop_index(op.f("ix_survey_description"), table_name="survey")
    op.drop_table("survey")
    # ### end Alembic commands ###
