"""Add data export Request Model

Revision ID: 20e83fa36885
Revises: 8b96bc287678
Create Date: 2021-12-04 15:18:40.727387

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '20e83fa36885'
down_revision = '8b96bc287678'
branch_labels = None
depends_on = None


def upgrade():
    # ### crete dataexport table  ###
    op.create_table(
        "dataexport",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("owner_id", sa.Integer(), nullable=True),
        sa.Column("survey_id", sa.Integer(), nullable=False),
        sa.Column("status", sa.String(), nullable=False, default='pending'),
        sa.Column("submited_at", sa.TIMESTAMP(), nullable=False),
        sa.Column("start_date", sa.TIMESTAMP()),
        sa.Column("end_date", sa.TIMESTAMP()),
        sa.ForeignKeyConstraint(["owner_id"], ["user.id"],),
        sa.ForeignKeyConstraint(["survey_id"], ["survey.id"],),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_index(op.f("ix_dataexport"), "dataexport", ["id"], unique=True)
    # ### end create dataexport table ###


def downgrade():
    # ### drop dataexport table ###
    op.drop_table("dataexport")
    # ### end drop dataexport table ###
