"""Create dataimport table

Revision ID: 926d609787b9
Revises: 20e83fa36885
Create Date: 2021-12-05 17:22:37.706348

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '926d609787b9'
down_revision = '20e83fa36885'
branch_labels = None
depends_on = None


def upgrade():
    # ### Create data import table ###
    op.create_table(
        "dataimport",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("owner_id", sa.Integer(), nullable=True),
        sa.Column("survey_id", sa.Integer(), nullable=False),
        sa.Column("status", sa.String(), nullable=False, default='pending'),
        sa.Column("submited_at", sa.TIMESTAMP(), nullable=False),
        sa.ForeignKeyConstraint(["owner_id"], ["user.id"],),
        sa.ForeignKeyConstraint(["survey_id"], ["survey.id"],),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_index(op.f("ix_dataimport"), "dataimport", ["id"], unique=True)
    # ### end Create data import table ###


def downgrade():
    # ### Drop data import table ###
    op.drop_table("dataimport")
    # ### Drop data import table ###
