from json.decoder import JSONDecodeError
from typing import Any, List

from sqlalchemy.sql.expression import null
from app.schemas.msg import Msg
import json
from app.schemas.feedback import FeedbackCreate

from fastapi import APIRouter, Depends, HTTPException, status, BackgroundTasks, UploadFile, File
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=List[schemas.Feedback])
def read_feedbacks(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve feedbacks.
    """
    if crud.user.is_superuser(current_user):
        feedbacks = crud.feedback.get_multi(db, skip=skip, limit=limit)
    else:
        feedbacks = crud.feedback.get_multi_by_owner(
            db=db, owner_id=current_user.id, skip=skip, limit=limit
        )
    return feedbacks


@router.get("/surveys/{survey_id}", response_model=List[schemas.Feedback])
def read_feedbacks_by_survey(
    survey_id: int,
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve survey feedbacks.
    """
    if crud.user.is_superuser(current_user):
        feedbacks = crud.feedback.get_multi_by_survey(
            db, survey_id=survey_id, skip=skip, limit=limit)
    else:
        feedbacks = crud.feedback.get_multi_by_owner_by_survey(
            db=db, survey_id=survey_id, owner_id=current_user.id, skip=skip, limit=limit
        )
    return feedbacks


@router.post("/", response_model=schemas.Feedback)
def create_feedback(
    *,
    db: Session = Depends(deps.get_db),
    feedback_in: schemas.FeedbackCreate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new feedback.
    """
    try:
        data = json.loads(feedback_in.data)
        if not isinstance(data, list):
            raise HTTPException(
                status_code=422, detail="feedback data is not a valid json array string")
    except JSONDecodeError:
        raise HTTPException(
            status_code=422, detail="feedback data is not a valid json string")

    feedback = crud.feedback.create_with_owner(
        db=db, obj_in=feedback_in, owner_id=current_user.id)
    return feedback


@router.post("/surveys/{survey_id}/batch", status_code=202)
async def create_feedback_batch(
    survey_id:int,
    *,
    db: Session = Depends(deps.get_db),
    feedback_in: schemas.FeedbackCreateBatch,
    current_user: models.User = Depends(deps.get_current_active_user),
        background_tasks: BackgroundTasks):
    """
    Create feedback in bacth.
    """

    crud.feedback.save_for_sync(db=db, obj_in=feedback_in, owner_id=current_user.id,survey_id=survey_id)

    return {"message": f"Your survey data are being imported in db"}


@router.post("/surveys/{survey_id}/import", status_code=202)
async def import_feedback_batch(
     survey_id: int,
    *,
    db: Session = Depends(deps.get_db),
    file: UploadFile = File(...),
    current_user: models.User = Depends(deps.get_current_active_user),
        background_tasks: BackgroundTasks):
    """
    Import survey feedback data.
    """

    background_tasks.add_task(crud.feedback.save_file_for_sync(
        db=db, fileb=file,survey_id=survey_id, owner_id=current_user.id))

    return {"message": f"Your survey data are being imported in db"}


@router.put("/{id}", response_model=schemas.Feedback)
def update_feedback(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    feedback_in: schemas.FeedbackUpdate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an feedback.
    """
    feedback = crud.feedback.get(db=db, id=id)
    if not feedback:
        raise HTTPException(status_code=404, detail="Feedback not found")
    if not crud.user.is_superuser(current_user) and (feedback.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    feedback = crud.feedback.update(db=db, db_obj=feedback, obj_in=feedback_in)
    return feedback


@router.get("/{id}", response_model=schemas.Feedback)
def read_feedback(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get feedback by ID.
    """
    feedback = crud.feedback.get(db=db, id=id)
    if not feedback:
        raise HTTPException(status_code=404, detail="Feedback not found")
    if not crud.user.is_superuser(current_user) and (feedback.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    return feedback


@router.delete("/{id}", response_model=schemas.Feedback)
def delete_feedback(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an feedback.
    """
    feedback = crud.feedback.get(db=db, id=id)
    if not feedback:
        raise HTTPException(status_code=404, detail="Feedback not found")
    if not crud.user.is_superuser(current_user) and (feedback.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    feedback = crud.feedback.remove(db=db, id=id)
    return feedback
