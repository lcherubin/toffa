import os
from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=List[schemas.Dataimport])
def read_data_imports(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve dataimports.
    """
    if crud.user.is_superuser(current_user):
        dataimports = crud.dataimport.get_multi(db, skip=skip, limit=limit)
    else:
        dataimports = crud.dataimport.get_multi_by_owner(
            db=db, owner_id=current_user.id, skip=skip, limit=limit
        )
    return dataimports



@router.get("/{id}", response_model=schemas.Dataimport)
def read_data_import(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get dataimport by ID.
    """
    dataimport = crud.dataimport.get(db=db, id=id)
    if not dataimport:
        raise HTTPException(status_code=404, detail="Dataimport not found")
    if not crud.user.is_superuser(current_user) and (dataimport.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    return dataimport


@router.delete("/{id}", response_model=schemas.Dataimport)
def delete_data_import(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an dataimport.
    """
    dataimport = crud.dataimport.get(db=db, id=id)
    if not dataimport:
        raise HTTPException(status_code=404, detail="Dataimport not found")
    if not crud.user.is_superuser(current_user) and (dataimport.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    dataimport = crud.dataimport.remove(db=db, id=id)
    return dataimport

