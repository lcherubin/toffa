import os
from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=List[schemas.Dataexport])
def read_data_exports(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve dataexports.
    """
    if crud.user.is_superuser(current_user):
        dataexports = crud.dataexport.get_multi(db, skip=skip, limit=limit)
    else:
        dataexports = crud.dataexport.get_multi_by_owner(
            db=db, owner_id=current_user.id, skip=skip, limit=limit
        )
    return dataexports


@router.post("/", response_model=schemas.Dataexport, status_code=202)
def create_data_export(
    *,
    db: Session = Depends(deps.get_db),
    dataexport_in: schemas.DataexportCreate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new dataexport.
    """
    dataexport = crud.dataexport.create_with_owner(
        db=db, obj_in=dataexport_in, owner_id=current_user.id)
    return dataexport


@router.put("/{id}", response_model=schemas.Dataexport)
def update_data_export(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    dataexport_in: schemas.DataexportUpdate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an dataexport.
    """
    dataexport = crud.dataexport.get(db=db, id=id)
    if not dataexport:
        raise HTTPException(status_code=404, detail="Dataexport not found")
    if not crud.user.is_superuser(current_user) and (dataexport.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    dataexport = crud.dataexport.update(db=db, db_obj=dataexport, obj_in=dataexport_in)
    return dataexport


@router.get("/{id}", response_model=schemas.Dataexport)
def read_data_export(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get dataexport by ID.
    """
    dataexport = crud.dataexport.get(db=db, id=id)
    if not dataexport:
        raise HTTPException(status_code=404, detail="Dataexport not found")
    if not crud.user.is_superuser(current_user) and (dataexport.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    return dataexport


@router.delete("/{id}", response_model=schemas.Dataexport)
def delete_data_export(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an dataexport.
    """
    dataexport = crud.dataexport.get(db=db, id=id)
    if not dataexport:
        raise HTTPException(status_code=404, detail="Dataexport not found")
    if not crud.user.is_superuser(current_user) and (dataexport.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    dataexport = crud.dataexport.remove(db=db, id=id)
    return dataexport


@router.get("/{id}/download", response_model=schemas.Dataexport)
def download_exported_data(
    *,
    db: Session = Depends(deps.get_db),
    id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
     ownload exported data.
    """
    dataexport = crud.dataexport.get(db=db, id=id)
    if not dataexport:
        raise HTTPException(status_code=404, detail="Dataexport not found")
    if not crud.user.is_superuser(current_user) and (dataexport.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    file_name = f"exports/survey_{dataexport.survey_id}_export_{dataexport.id}_.csv"

    file_path = os.getcwd() + "/" + file_name

    if not os.path.isfile(file_path):
        raise HTTPException(status_code=404, detail="Dataexport not found")
    return FileResponse(path=file_path, media_type='application/octet-stream', filename=file_name)
