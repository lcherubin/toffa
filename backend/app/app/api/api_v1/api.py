from app.models import survey
from fastapi import APIRouter

from app.api.api_v1.endpoints import login, users, utils, surveys, questions, feedbacks, dataexports, dataimports

api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(utils.router, prefix="/utils", tags=["utils"])
api_router.include_router(surveys.router, prefix="/surveys", tags=["surveys"])
api_router.include_router(questions.router, prefix="/questions", tags=["questions"])
api_router.include_router(feedbacks.router, prefix="/feedbacks", tags=["feedbacks"])
api_router.include_router(dataexports.router, prefix="/dataexports", tags=["dataexports"])
api_router.include_router(dataimports.router, prefix="/dataimports", tags=["dataimports"])
