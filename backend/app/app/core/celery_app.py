from celery import Celery

celery_app = Celery("worker", broker="amqp://guest@queue//")

celery_app.conf.task_routes = {
    "app.worker.test_celery": "main-queue",
    "app.worker.sync_feedback": "main-queue",
    "app.verify_data_export_request": "main-queue",
    "app.export_feedback_to_csv": "main-queue"
}



