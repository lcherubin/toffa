from .crud_item import item
from .crud_user import user
from .crud_survey import survey
from .crud_question import question
from .crud_feedback import feedback
from .crud_dataexport import dataexport
from .crud_dataimport import dataimport
# For a new basic set of CRUD operations you could just do

# from .base import CRUDBase
# from app.models.item import Item
# from app.schemas.item import ItemCreate, ItemUpdate

# item = CRUDBase[Item, ItemCreate, ItemUpdate](Item)
