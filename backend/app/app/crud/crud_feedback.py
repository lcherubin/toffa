from datetime import datetime
import os
from typing import Any, List
import json

from sqlalchemy.sql.functions import now
from app.worker import sync_feedback
from app.core.celery_app import celery_app
from app.models.dataimport import Dataimport
from app.models import dataimport
from fastapi import File, UploadFile
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from uuid import UUID, uuid4
from app.crud.base import CRUDBase
from app.models.feedback import Feedback
from app.schemas.feedback import FeedbackCreate, FeedbackCreateBatch, FeedbackUpdate


class CRUDFeedback(CRUDBase[Feedback, FeedbackCreate, FeedbackUpdate]):
    def create_with_owner(
        self, db: Session, *, obj_in: FeedbackCreate, owner_id: int
    ) -> Feedback:

        if obj_in.submited_at is None:
            obj_in.submited_at = datetime.now()

        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def save_for_sync(
        self, db: Session, *, obj_in: FeedbackCreateBatch, owner_id: int, survey_id: int
    ) -> Any:
        """ save feedback in json file and import them in db later

        Args:
            db (Session): [description]
            obj_in (FeedbackCreateBatch): [description]
            owner_id (int): [description]

        Returns:
            Any: [description]
        """
        import_obj = self.create_import(db=db, owner_id=owner_id, survey_id=survey_id)
        filename = f"sync/feedbacks/import_{import_obj.id}__feedbacks__.json"

        content = jsonable_encoder(obj_in)

        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)

            with open(f"{filename}", mode="w") as feedback_file:
                feedback_file.write(json.dumps(content))
        except Exception as e:
            print(e)
        sync_feedback.apply_async(args=[filename, import_obj.id])
        return {"message": "success"}

    def save_file_for_sync(self, *, db: Session, fileb: File, survey_id: int, owner_id: int):
        '''
        Save file and call import waker method.
        '''
        import_obj = self.create_import(db=db, owner_id=owner_id, survey_id=survey_id)
        name, extension = os.path.splitext(fileb.filename)
        filename = f"sync/feedbacks/import_{import_obj.id}__feedbacks__{extension}".replace(
            ' ', '-')
        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            print(os.getcwd())
            with open(filename, 'wb+') as f:
                f.write(fileb.file.read())
                f.close()

            sync_feedback.apply_async(args=[filename, import_obj.id])
        except Exception as e:
            print(e)

        return {"message": "success"}

    def get_multi_by_owner(
        self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Feedback]:
        '''Get feedbacks by owner
        '''
        return (
            db.query(self.model)
            .filter(Feedback.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    async def get_multi_by_survey(
        self, db: Session, *, survey_id: int, skip: int = 0, limit: int = 100
    ) -> List[Feedback]:
        return (
            db.query(self.model)
            .filter(Feedback.survey_id == survey_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    async def get_multi_by_owner_by_survey(
        self, db: Session, *, survey_id, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Feedback]:
        return (
            db.query(self.model)
            .filter(Feedback.survey_id == survey_id)
            .filter(Feedback.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def create_import(self, db: Session, *, owner_id: int, survey_id: int):
        import_obj = Dataimport()
        import_obj.owner_id = owner_id
        import_obj.status = 'pending'
        import_obj.survey_id = survey_id
        import_obj.submited_at = datetime.now()
        db.add(import_obj)
        db.commit()
        db.refresh(import_obj)
        return import_obj


feedback = CRUDFeedback(Feedback)
