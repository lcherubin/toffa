from datetime import datetime
from typing import List
from app.worker import export_feedback_to_csv

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.dataexport import Dataexport
from app.schemas.dataexport import DataexportCreate, DataexportUpdate


class CRUDDataexport(CRUDBase[Dataexport, DataexportCreate, DataexportUpdate]):
    def create_with_owner(
        self, db: Session, *, obj_in: DataexportCreate, owner_id: int
    ) -> Dataexport:
        obj_in.submited_at = datetime.now()
        obj_in.status = 'pending'
        obj_in.owner_id = owner_id
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        export_feedback_to_csv.apply_async(args=[db_obj.id])
        # export_feedback_to_csv(db_obj.id)
        return db_obj

    def get_multi_by_owner(
        self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Dataexport]:
        return (
            db.query(self.model)
            .filter(Dataexport.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )



dataexport = CRUDDataexport(Dataexport)
