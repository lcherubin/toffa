from datetime import datetime
from typing import List
from app.worker import export_feedback_to_csv

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.dataimport import Dataimport
from app.schemas.dataimport import DataimportCreate, DataimportUpdate


class CRUDDataimport(CRUDBase[Dataimport, DataimportCreate, DataimportUpdate]):
    def create_with_owner(
        self, db: Session, *, obj_in: DataimportCreate, owner_id: int
    ) -> Dataimport:
        obj_in.submited_at = datetime.now()
        obj_in.status = 'pending'
        obj_in.owner_id = owner_id
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_multi_by_owner(
        self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Dataimport]:
        return (
            db.query(self.model)
            .filter(Dataimport.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )



dataimport = CRUDDataimport(Dataimport)
