from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.db.base_class import Base

if TYPE_CHECKING:
    from .user import User  # noqa: F401
    from .question import Question  # noqa: F401


class Survey(Base):
    ''' Survey Model, 
        holds information about the imported data survey.
    '''
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="surveys")
    questions = relationship("Question", back_populates="survey")
    feedbacks = relationship("Feedback", back_populates="survey")
    dataexports = relationship("Dataexport", back_populates="survey")
    dataimports = relationship("Dataimport", back_populates="survey")
