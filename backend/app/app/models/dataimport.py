from typing import TYPE_CHECKING

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DATETIME, JSON, DateTime

from app.db.base_class import Base

if TYPE_CHECKING:
    from .user import User  # noqa: F401
    from .survey import Survey  # noqa: F401


class Dataimport(Base):
    ''' Dataimport Model, 
        holds information about the imported survey data.
    '''
    id = Column(Integer, primary_key=True, index=True)
    status = Column(String)
    submited_at = Column(DATETIME, index=True)
    survey_id = Column(Integer, ForeignKey("survey.id"))
    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="dataimports")
    survey = relationship("Survey", back_populates="dataimports")
