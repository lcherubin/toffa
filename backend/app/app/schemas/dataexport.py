from typing import List, Optional
from .survey import SurveyBase as Surevy
from datetime import datetime
from pydantic import BaseModel


# Shared properties
class DataexportBase(BaseModel):
    survey_id: Optional[int] = None
    submited_at: Optional[datetime] = None
    owner_id: Optional[int] = None
    start_date: Optional[datetime] = None
    end_date: Optional[datetime] = None
    status: Optional[str] = None


# Properties to receive on dataexport creation
class DataexportCreate(DataexportBase):
    pass


# Properties to receive on dataexport update
class DataexportUpdate(DataexportBase):
    pass


# Properties shared by models stored in DB
class DataexportInDBBase(DataexportBase):
    id: int
    survey_id: int
    submited_at: datetime
    owner_id: int
    start_date: datetime
    end_date: datetime
    status: str

    class Config:
        orm_mode = True


# Properties to return to client
class Dataexport(DataexportInDBBase):
    pass


# Properties properties stored in DB
class DataexportInDB(DataexportInDBBase):
    pass
