from typing import List, Optional
from .survey import SurveyBase as Surevy
from datetime import datetime
from pydantic import BaseModel


# Shared properties
class DataimportBase(BaseModel):
    survey_id: Optional[int] = None
    submited_at: Optional[datetime] = None
    owner_id: Optional[int] = None
    status: Optional[str] = None


# Properties to receive on dataimport creation
class DataimportCreate(DataimportBase):
    pass


# Properties to receive on dataimport update
class DataimportUpdate(DataimportBase):
    pass


# Properties shared by models stored in DB
class DataimportInDBBase(DataimportBase):
    id: int
    survey_id: int
    submited_at: datetime
    owner_id: int
    status: str

    class Config:
        orm_mode = True


# Properties to return to client
class Dataimport(DataimportInDBBase):
    pass


# Properties properties stored in DB
class DataimportInDB(DataimportInDBBase):
    pass
