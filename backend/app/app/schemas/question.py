from typing import Optional

from pydantic import BaseModel


# Shared properties
class QuestionBase(BaseModel):
    title: Optional[str] = None
    slug: Optional[str] = None
    survey_id: int


# Properties to receive on question creation
class QuestionCreate(QuestionBase):
    title: str


# Properties to receive on question update
class QuestionUpdate(QuestionBase):
    pass


# Properties shared by models stored in DB
class QuestionInDBBase(QuestionBase):
    id: int
    title: str
    owner_id: int

    class Config:
        orm_mode = True


# Properties to return to client
class Question(QuestionInDBBase):
    pass


# Properties properties stored in DB
class QuestionInDB(QuestionInDBBase):
    pass
