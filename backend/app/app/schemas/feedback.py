from typing import List, Optional
from .survey import SurveyBase as Surevy
from datetime import datetime
from pydantic import BaseModel


# Shared properties
class FeedbackBase(BaseModel):
    data: Optional[str] = None
    survey_id: Optional[int] = None
    submited_at: Optional[datetime] = None
    owner_id: Optional[int] = None



# Properties to receive on feedback creation
class FeedbackCreate(FeedbackBase):
    pass

class FeedbackCreateBatch(BaseModel):
    survey_data : List[FeedbackCreate]


# Properties to receive on feedback update
class FeedbackUpdate(FeedbackBase):
    pass


# Properties shared by models stored in DB
class FeedbackInDBBase(FeedbackBase):
    id: int
    data: str
    owner_id: int
    survey_id: int
    submited_at: datetime

    class Config:
        orm_mode = True


# Properties to return to client
class Feedback(FeedbackInDBBase):
    pass


# Properties properties stored in DB
class FeedbackInDB(FeedbackInDBBase):
    pass
