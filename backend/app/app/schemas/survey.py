from typing import List, Optional
from .question import QuestionBase as Question

from pydantic import BaseModel


# Shared properties
class SurveyBase(BaseModel):
    title: Optional[str] = None
    # slug: Optional[str] = None
    # questions: List[Question] = []

# Properties to receive on survey creation
class SurveyCreate(SurveyBase):
    title: str


# Properties to receive on survey update
class SurveyUpdate(SurveyBase):
    pass


# Properties shared by models stored in DB
class SurveyInDBBase(SurveyBase):
    id: int
    title: str
    owner_id: int

    class Config:
        orm_mode = True


# Properties to return to client
class Survey(SurveyInDBBase):
    pass


# Properties properties stored in DB
class SurveyInDB(SurveyInDBBase):
    pass
