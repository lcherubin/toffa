
import os
import time
from datetime import date, datetime
import json
import csv
from uuid import uuid4
from app.models.feedback import Feedback
from app.models.dataexport import Dataexport
from app.models.dataimport import Dataimport
from raven import Client


from app.db.session import SessionLocal
from app.core.celery_app import celery_app
from app.core.config import settings

client_sentry = Client(settings.SENTRY_DSN)


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls verify_data_export_request() every 60 seconds.
    sender.add_periodic_task(60.0, verify_data_export_request.s(), name='add every 60')
    

@celery_app.task(acks_late=True)
def verify_data_export_request():
    print(f"verifying verify_data_export_request at {datetime.now()}")
    db = SessionLocal()
    pending_requests = (db.query(Dataexport)
                        .filter(Dataexport.status == 'pending')
                        .all())
    print(f" {len(pending_requests)} pending request(s) found")
    for req in pending_requests:
        export_feedback_to_csv.apply_async(args=[req.id])


@celery_app.task(acks_late=True)
def test_celery(word: str) -> str:
    return f"test task return {word}"


@celery_app.task(acks_late=True,)
def sync_feedback(filename: str, import_id: int):
    ''' 
        Read the feedbacks file, proccess the imported survey data and save to db
    '''

    db = SessionLocal()

    request = (db.query(Dataimport)
                 .get(import_id))

    if request is None or request.status != 'pending':
        print(f'no pending import request for id = {import_id}')
        return
    try:
        print(f"start sync feedback at {datetime.now()}")
        with open(f"{filename}", mode="r") as feedback_file:
            print('processing==========================')
            data = json.load(feedback_file)
            request.status = 'precessing'
            db.add(request)
            db.commit()
            db.refresh(request)
            for survey_data in data['survey_data']:
                feedback = Feedback()
                feedback.survey_id = request.survey_id
                feedback.data = survey_data['data']
                feedback.owner_id = request.owner_id
                feedback.submited_at = request.submited_at
                db.add(feedback)
                db.commit()
        request.status = 'processed'
        db.add(request)
        db.commit()
        db.refresh(request)
    except Exception:
        request.status = 'failed'
        db.add(request)
        db.commit()
        db.refresh(request)
    print(f"finish at sync feedback at {datetime.now()}")


@celery_app.task(acks_late=True,)
def export_feedback_to_csv(request_id: int):
    ''' 
        Process the data export request 
    '''

    # field names
    fields = []
    # data rows of csv file
    rows = []

    db = SessionLocal()

    request = (db.query(Dataexport)
                 .get(request_id))

    if request is None or request.status != 'pending':
        print(f'no pending export request for id = {request_id}')
        return

    print(f"data export at {datetime.now()}")

    filename = f"exports/survey_{request.survey_id}_export_{request.id}_.csv"
    feebacks = (db.query(Feedback)
                  .filter(Feedback.submited_at.between(request.start_date, request.end_date))
                  .all())
    data = []
    request.status = 'processing'
    db.add(request)
    db.commit()
    db.refresh(request)
    # convert feedback data (json string) into dict (key,value object)
    # and extract questions for feedback data
    for f in feebacks:
        tab = json.loads(f.data)
        obj = {}
        for o in tab:
            if o['question'] not in fields:
                fields.append(o['question'])
            obj[o['question']] = o['feedback']
            data.append(obj)

    # create csv rows from feedback dict
    for d in data:
        row = []
        for q in fields:
            try:
                row.append(d[q])
            except KeyError:
                # add empty string if no feedback for the question
                row.append("")
        rows.append(row)

    try:
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        with open(filename, 'w') as f:

            write = csv.writer(f)
            write.writerow(fields)
            write.writerows(rows)
        print(f"finish data export at {datetime.now()}")
        request.status = 'processed'
        db.add(request)
        db.commit()
        db.refresh(request)
    except Exception as e:
        request.status = 'failed'
        db.add(request)
        db.commit()
        db.refresh(request)
        print(e)
