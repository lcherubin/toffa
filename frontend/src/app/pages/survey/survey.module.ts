import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzSharedModules } from './../../shared/nz-shared.module';
import { IconsProviderModule } from '../../icons-provider.module';
import { NgModule } from '@angular/core';

import { SurveyRoutingModule } from './survey-routing.module';

import { SurveyComponent } from './survey.component';
import { DataImportComponent } from './data-import/data-import.component';
import { DataExportComponent } from './data-export/data-export.component';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { DataImportModalComponent } from './data-import-modal/data-import-modal.component';
import { DataExportModalComponent } from './data-export-modal/data-export-modal.component';


@NgModule({
  imports: [
    CommonModule,
    SurveyRoutingModule,
    IconsProviderModule,
    FormsModule,
    ReactiveFormsModule,
    NzSharedModules,
  ],
  declarations: [SurveyComponent, DataImportComponent, DataExportComponent, FeedbacksComponent, DataImportModalComponent, DataExportModalComponent],
  exports: [SurveyComponent]
})
export class SurveyModule { }

export const STATUS_COLORS: any = {
  'pending': 'gold',
  'processing': 'orange',
  'processed': 'green',
  'failed': 'red'
}
