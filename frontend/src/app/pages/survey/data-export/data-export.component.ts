
import { AppService } from './../../../app.service';
import { DataExportModalComponent } from './../data-export-modal/data-export-modal.component';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { STATUS_COLORS } from '../survey.module';

@Component({
  selector: 'app-data-export',
  templateUrl: './data-export.component.html',
  styleUrls: ['./data-export.component.scss']
})
export class DataExportComponent implements OnInit, OnDestroy {

  private exportSub: any;
  public listOfData: any[] = [];
  colors = STATUS_COLORS
  constructor(private modalService: NzModalService, private notificationService: NzNotificationService, private appService: AppService) { }


  ngOnInit(): void {
    this.exportSub = this.appService.getAllExportRequests()
      .subscribe(
        (data) => {
          this.listOfData = data
        },
        (err) => {
          console.log(err);
        }
      )
  }

  showExport(): void {
    this.modalService.create({
      nzTitle: 'Export survey data',
      nzContent: DataExportModalComponent
    });
  }

  downlod(id: number) {
    this.appService.downloadExportedData(id)
  }

  ngOnDestroy() {

    if (this.exportSub != null && !this.exportSub.closed) {
      this.exportSub.unsubscribe();
    }
  }


}
