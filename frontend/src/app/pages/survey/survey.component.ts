import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from './../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {
  isCollapsed = false;


  constructor(private cookieService: CookieService, private router: Router) {
    //constructor
  }

  ngOnInit(): void {

  }

  public logout() {
      this.cookieService.delete('access_token', '/')
      this.router.navigateByUrl("/auth")
  }
}
