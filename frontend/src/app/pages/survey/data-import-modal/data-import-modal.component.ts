import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
declare let $: any;
@Component({
  selector: 'app-data-import-modal',
  templateUrl: './data-import-modal.component.html',
  styleUrls: ['./data-import-modal.component.scss']
})
export class DataImportModalComponent implements OnInit {
  surveys: any[] = []
  importForm!: FormGroup
  file: any
  constructor(private modal: NzModalRef, private msg: NzMessageService, private fb: FormBuilder, private appService: AppService) { }

  ngOnInit(): void {
    this.importForm = this.fb.group({
      survey: [null, [Validators.required]],
      file: [null, [Validators.required]],
    });

    this.appService.getAllSurveys().subscribe(
      (data) => {
        this.surveys = data
        // console.log(this.surveys)
      }
    )

  }

  public fileChanged(event: any) {
    var target = event.target || event.srcElement;
    this.file = target.files[0];
    console.log(this.file)
  }

  submit(): void {
    console.log(this.importForm.value)
    this.appService.importFeebacks(
      this.importForm.value.survey,
      $("#import")[0].files[0]
    ).subscribe(
      (data: any) => {
        // console.log(data)
        this.msg.info(data.message)
        this.modal.destroy();
      }
    )

  }

}
