import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { DataExportComponent } from './data-export/data-export.component';
import { DataImportComponent } from './data-import/data-import.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SurveyComponent } from './survey.component';

const routes: Routes = [
  {
    path: '',
    component: SurveyComponent,
    children: [
      { path: '', redirectTo: 'data-import', pathMatch: 'full' },
      { path: 'feedbacks', component: FeedbacksComponent },
      { path: 'data-import', component: DataImportComponent },
      { path: 'data-export', component: DataExportComponent },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveyRoutingModule { }
