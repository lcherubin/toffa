import { AppService } from './../../../app.service';
import { STATUS_COLORS } from './../survey.module';
import { DataImportModalComponent } from './../data-import-modal/data-import-modal.component';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-data-import',
  templateUrl: './data-import.component.html',
  styleUrls: ['./data-import.component.scss']
})
export class DataImportComponent implements OnInit {
  private sub: any;
  public listOfData: any[] = [];
  colors = STATUS_COLORS

  constructor(private modalService: NzModalService, private appService:AppService) { }

  ngOnInit(): void {
    this.sub = this.appService.getAllImportRequests()
    .subscribe(
      (data) => {
        this.listOfData = data
      },
      (err) => {
        console.log(err);
      }
    )
  }

  showImport(): void {
    this.modalService.create({
      nzTitle: 'Import survey data',
      nzContent: DataImportModalComponent
    });
  }


}
