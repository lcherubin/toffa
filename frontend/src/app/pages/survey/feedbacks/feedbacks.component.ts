import { AppService } from './../../../app.service';
import { DataExportModalComponent } from './../data-export-modal/data-export-modal.component';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DataImportModalComponent } from './../data-import-modal/data-import-modal.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feedbacks',
  templateUrl: './feedbacks.component.html',
  styleUrls: ['./feedbacks.component.scss']
})
export class FeedbacksComponent implements OnInit {
  btnSize = 'large'
  surveyForm!: FormGroup;
  listOfData: any[] = [];
  headers: any[] = [];
  public surveys: any[] = []

  constructor(private fb: FormBuilder, private modalService: NzModalService, private appService: AppService) { }

  ngOnInit(): void {
    this.surveyForm = this.fb.group({
      survey: [null, [Validators.required]]
    });

    // for (let i = 0; i < 100; i++) {
    //   this.listOfData.push({
    //     name: `Edward King ${i}`,
    //     age: 32,
    //     address: `London`
    //   });
    // }


    this.appService.getAllSurveys().subscribe(
      (data) => {
        this.surveys = data
      }
    )

    this.appService.getAllFeedbacks().subscribe(
      (resp: any) => {
        // console.log(data)

        // let objRow: any[] = []
        // let dataObj: any = []
      //   resp.forEach((element: any) => {
      //     element.data = JSON.parse(element.data)
      //     let fobj: any = {}
      //     element.data.forEach((d: any) => {

      //       let h: any = d['question']
      //       if (!h) {
      //         h = d['Question']
      //       }
      //       if (!this.headers.includes(h)) {
      //         this.headers.push(h);
      //       }
      //       fobj[h] = d['feedback']
      //     });
      //     dataObj.push(fobj)


      //     dataObj.forEach((d: any) => {
      //       let row: any[] = [];
      //       this.headers.forEach(q => {
      //         let feedback = d[q]
      //         if (feedback) {
      //           row.push(feedback)
      //         } else {
      //           row.push("")
      //         }
      //       })
      //       this.listOfData.push(row)
      //     })
      //   });
      //   console.log(this.headers)

      }
    )
  }

  submitForm(): void {
    if (this.surveyForm.valid) {
      console.log('submit', this.surveyForm.value);
    } else {
      Object.values(this.surveyForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  showImport(): void {
    this.modalService.create({
      nzTitle: 'Import survey data',
      nzContent: DataImportModalComponent
    });
  }

  showExport(): void {
    this.modalService.create({
      nzTitle: 'Export survey data',
      nzContent: DataExportModalComponent
    });
  }



  surveyChange(value: string): void {
    // this.surveyForm.get('note')!.setValue(value === 'male' ? 'Hi, man!' : 'Hi, lady!');
  }

}
