import { NzNotificationService } from 'ng-zorro-antd/notification';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-export-modal',
  templateUrl: './data-export-modal.component.html',
  styleUrls: ['./data-export-modal.component.scss']
})
export class DataExportModalComponent implements OnInit {

  exportForm!: FormGroup;

  public surveys: any[] = []

  constructor(private fb: FormBuilder, private appService: AppService,
    private modal: NzModalRef, private notifificationService: NzNotificationService) {
    // constructor
  }

  ngOnInit(): void {
    this.exportForm = this.fb.group({
      survey: [null, [Validators.required]],
      dates: [null, [Validators.required]],
    });

    this.appService.getAllSurveys().subscribe(
      (data) => {
        this.surveys = data
      }
    )
  }

  submit() {
    const data = this.exportForm.value;
    const body = {
      "survey_id": data.survey,
      "start_date": `${new Date(data.dates[0]).toISOString()}`,
      "end_date": `${new Date(data.dates[1]).toISOString()}`
    }

    this.appService.submitExportRequest(body).subscribe(
      (d) => {
        this.notifificationService.info('Success', `<p>Your request has been accepted</p>`)
        this.modal.destroy();
      }
    )
  }


  onChange(result: Date): void {
    // console.log('Selected Time: ', result);
  }

  onOk(result: Date | Date[] | null): void {
    // console.log('onOk', result);
  }

  onCalendarChange(result: Array<Date | null>): void {
    // console.log('onCalendarChange', result);
  }

}
