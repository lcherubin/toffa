import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataExportModalComponent } from './data-export-modal.component';

describe('DataExportModalComponent', () => {
  let component: DataExportModalComponent;
  let fixture: ComponentFixture<DataExportModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataExportModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataExportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
