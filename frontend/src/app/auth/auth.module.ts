import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzSharedModules } from './../shared/nz-shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';



@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzSharedModules,
    AuthRoutingModule
  ]
})
export class AuthModule { }
