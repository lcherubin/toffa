import { AppService } from './../../app.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm!: FormGroup;
  public errorMsg = "";
  public loginFailed = false;
  private loginSub: any;

  constructor(private fb: FormBuilder, private router: Router, private appService: AppService, private cookieService: CookieService) { }



  ngOnInit(): void {
    this.loginForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }


  submitForm(): void {
    if (this.loginForm.valid) {
      this.loginSub = this.appService.login(this.loginForm.value.userName, this.loginForm.value.password)
        .subscribe(
          (token) => {
            this.loginFailed = false
            this.cookieService.set('access_token', token.access_token)
            this.router.navigate(['/'])
          },
          (err) => {
            this.loginFailed = true
            this.errorMsg = err?.error?.detail
          }
        )

    } else {
      Object.values(this.loginForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }


  ngOnDestroy(): void {
    if (this.loginSub != null) {
      this.loginSub.unsubscribe();
    }
  }


}
