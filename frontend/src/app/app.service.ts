import { environment } from './../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  baseUrl = `${environment.apiUrl}/v1`

  constructor(private httpClient: HttpClient) { }


  public login(username: string, password: string): Observable<any> {
    const body = new HttpParams()
      .set('username', username)
      .set('password', password);
    return this.httpClient.post(`${this.baseUrl}/login/access-token`,
      body.toString(),
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      }
    );
  }


  /**
   * Get all export request
   * @param skip
   * @param limit
   * @returns
   */
  public getAllExportRequests(skip?: number, limit?: number): Observable<any> {
    if (skip != undefined && limit != undefined) {
      return this.httpClient.get(`${this.baseUrl}/dataexports/?skip=${skip}&limit=${limit}`);
    }
    return this.httpClient.get(`${this.baseUrl}/dataexports/?skip=0&limit=2000`);

  }

  /**
   * Download exported data
   * @param requestId
   * @returns
   */
  public downloadExportedData(requestId: number) {
    return this.httpClient.get(`${this.baseUrl}/dataexports/${requestId}/download`,
      { responseType: 'blob' as 'json' }
    ).subscribe(
      (response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
        downloadLink.setAttribute('download', `exported_data_${requestId}`);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }
    )

  }




  /**
   * Submit new data export request
   * @param body
   * @returns
   */
  public submitExportRequest(body: { survey_id: number; start_date: string; end_date: string; }) {
    return this.httpClient.post(`${this.baseUrl}/dataexports`, body)
  }

  /**
   *
   * @param skip
   * @param limit
   * @returns
   */
  public getAllImportRequests(skip?: number, limit?: number): Observable<any> {
    if (skip != undefined && limit != undefined) {
      return this.httpClient.get(`${this.baseUrl}/dataimports/?skip=${skip}&limit=${limit}`);
    }
    return this.httpClient.get(`${this.baseUrl}/dataimports/?skip=0&limit=2000`);
  }
  /**
   * import survey data
   *
   */
  public importFeebacks(survey_id: number, file: any) {
    let formData: FormData = new FormData();
    formData.append("file", file);
    return this.httpClient.post(`${this.baseUrl}/feedbacks/surveys/${survey_id}/import`, formData);
  }
  /**
   * Get all export request
   * @param skip
   * @param limit
   * @returns
   */
  public getAllSurveys(skip?: number, limit?: number): Observable<any> {
    if (skip != undefined && limit != undefined) {
      return this.httpClient.get(`${this.baseUrl}/surveys/?skip=${skip}&limit=${limit}`);
    }
    return this.httpClient.get(`${this.baseUrl}/surveys/?skip=0&limit=2000`);

  }
  /**
   *
   * @param survey_id
   * @param skip
   * @param limit
   * @returns
   */
  public getAllFeedbacks(survey_id?: number, skip?: number, limit?: number) {

    if (survey_id != null) {
      return this.httpClient.get(`${this.baseUrl}/feedbacks/survey/${survey_id}/?skip=0&limit=2000`);
    }
    return this.httpClient.get(`${this.baseUrl}/feedbacks/?skip=0&limit=2000`);
  }

}
